;
; This file is part of WPS-PC.
; Copyright (C) 1996 - 2018 Canux Corporation
;
; WPS-PC is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; WPS-PC is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with WPS-PC.  If not, see <http://www.gnu.org/licenses/>.
;
;*********************************************************************
;
;	IBM PC KEYBOARD ISR AND BUFFERING ROUTINE 
;
; Copyright (c) 1985 Exceptional Business Solutions, Inc.
;
;*********************************************************************

	NAME	IBMKBD	 ;MODULE NAME

;MODULE HISTORY

;4/25/86 012 - Moved UPD_KBD to invoke only on change of CAPS LOCK, not
;              any shift change.

PAGE
	INCLUDE DOSMAC.ASM
	INCLUDE	WPSMAC.ASM

	PUBLIC	KB_FLAG,INIT_KB,KBD_CHAR,RESET_KB,HALT_FG,RULR_FG
	PUBLIC	BEEP$$,SET_EVENT$$,CLR_EVENT$$
	PUBLIC	BUF_HEAD,BUF_TAIL
	PUBLIC	CTRL_SHIFT

	EXTRN	PRINTER_BUSY:BYTE
	EXTRN	KB5151:BYTE
	EXTRN	SCREEN_MODE:BYTE
	EXTRN	STATUS_FLAG:BYTE
	EXTRN	VIDBASE:WORD
	EXTRN	DISP_CHAR:NEAR

DATA     SEGMENT	PUBLIC 'DATA'

;LOCAL DEFINITIONS

CPSLK_BRK =	0BAH	;CAPS LOCK BREAK
CTRL_MAK =	01DH	;CONTROL MAKE
CTRL_BRK =	09DH	;CONTROL BREAK
LSHFT_MAK =	02AH	;LEFT SHIFT MAKE
LSHFT_BRK =	0AAH	;LEFT SHIFT BREAK
RSHFT_MAK =	036H	;RIGHT SHIFT MAKE
RSHFT_BRK =	0B6H	;RIGHT SHIFT BREAK
ALT_MAK	=	038H	;ALT MAKE
ALT_BRK	=	0B8H	;ALT BREAK


KB_DATA	=	60H	;KBD DATA PORT
SYS_CTL	=	61H	;KBD CONTROL PORT
INT_CTRL =	20H	;INTERRUPT CONTROL PORT
EOI	=	20H
HALT_KEY =	20	;HELP/HALT KEY INTERNAL CODE
DEL_KEY	=	26	;DELETE KEY INTERNAL CODE
NP1_KEY	=	11	;NUM PAD 1 INTERNAL CODE
NP5_KEY	=	15	;NUM PAD 5 INTERNAL CODE

;KEYBOARD FLAG BIT DEFINITIONS, 1=ON, 0=OFF

CPSLK_FG =	1	;CAPS LOCK MODE
CTRL_FG	=	2	;CONTROL KEY
ALT_FG	=	4	;ALT KEY
SHFT_FG	=	8	;EITHER LEFT OR RIGHT SHIFT
GOLD_FG	=	20H	;GOLD KEY SEQUENCE

KB_BUF	DW	30 DUP (?) ;KEYBOARD BUFFER
KB_BUF_END	LABEL	WORD

BUF_HEAD	DW	0	;POINTS TO NEXT CHAR TO REMOVE FROM BUFFER
BUF_TAIL	DW	0	;POINTS TO LAST CHAR STORED IN BUFFER	


;TIMER & TONE DEFINITIONS

TONE	=	1355	;880 Hz
BP_LEN	=	7	;NUMBER OF CLOCK TICKS IN BEEP
TONE_CTL =	0B6H	;TIMER 2, 1 DATA BYTE PER, SQUARE WAVE, BINARY
TIMR_DATA =	42H	;TIMER DATA PORT
TIMR_CTL =	43H	;TIMER CONTROL PORT

BEEPS	DB	0	;CLOCK TICK COUNT REMAINING FOR BEEP

;EVENT TABLE

EVENT_TBL LABEL WORD
	DD	0	;EVENT 1
	DD	0	;EVENT 2
	DD	0	;EVENT 3

NUM_EVENTS EQU	3

OLD_KBD	DW	0,0	;SAVED KB INTERRUPT VECTOR
OLD_CLK DW	0,0	;SAVED CLOCK INTERRUPT VECTOR
KB_FLAG	DB	0	;INITIAL KEYBOARD STATE
HALT_FG	DB	0	;HALT FLAG
ADIGIT	DB	0	;ALT-KEY ASCII CHAR
GDIGIT	DB	0	;GOLD NUMBER
RULR_FG	DB	0	;RULER MODE, 1=IN RULKER MODE
DIG_FG	DB	0	;1=GOLD DIGIT, 0=NONE
G_N_G_FG DB	0	;1=GOLD-n-GOLD, 0=NOT (MAY OR MAY NOT HAVE n)
CTRL_SHIFT DB	0	;1=TREAT CTRL KEY AS SHIFT LOCK, 0=NORMAL
CPU_TYPE DB	0	;IBM CPU TYPE, FOUND AT FFFF:0E, AT = 0FCH

PAGE
;INITIAL SCAN CODE TO INTERNAL FORMAT TRANSLATION TABLE

XLINT	DB	0,20,49,50,51,52,53,54,55,56,57,48,30,31,21,32,81,87,69
	DB	82,84,89,85,73,79,80,33,34,35,2,65,83,68,70,71,72,74,75,76
	DB	36,37,38,4,39,90,88,67,86,66,78,77,40,41,42,5,22,3,43,1
	DB	128,129,130,131,132,133,134,135,136,137,0,23,17,18,19,24
	DB	14,15,16,25,11,12,13,10,26

;ALPHA TO GOLD FUNCTION TRANSLATION TABLE (IN ALPHABETIC ORDER)

ALFG_TBL	DB	G_SUB,G_BOT,G_CNTR,G_OVS,0,G_FILE,G_GETDOC,G_HELP
	DB	G_INDEX,0,G_QUIT,G_LIB,G_MENU,G_NEWP,G_OOPS,G_PGMARK
	DB	G_SUPER,G_RULER,0,G_TOP,G_UDK,G_VIEW,0,0,0,G_STATUS



;WORD STAR TRANSLATION TABLE FOR CTRL-ALPHA CURSOR MOTION
;(IN ALPHABETIC ORDER)

WS_TBL	DB	G_BAKW,0,G_PGDN,G_ADV,G_UP,G_ADVW,G_DELC,0,0,G_HELP,G_FILE
	DB	G_CONTS,0,0,G_RULER,0,G_XON,G_PGUP,G_XOF,G_DELW,G_UDK,0,0,G_DN
	DB	G_RUBL,G_CTRLZ



;DIGITS TO SHIFTED-DIGITS TRANS TABLE

DIGS_TBL	DB	")!@#$%^&*("



;SPECIAL WPS FUNCTION KEYS TRANS TABLE IN THIS ORDER:
;HELP, RUBC, RUBW, PAGE, CUT, PASTE AND SEL.

;UNSHIFTED

SPLN_TBL	DB	G_HELP,G_RUBC,G_RUBW,G_PAGE,G_CUT,G_PASTE,G_SEL

;SHIFTED

SPLS_TBL	DB	G_HELP,G_DELC,G_DELW,0,0,0,G_ANGLE

;GOLD

SPLG_TBL	DB	G_HALT,G_RUBL,G_RUBS,G_GPAGE,G_GCUT,G_GPASTE,G_SWAP



;PUNCTUATION KEY TRANS TABLES.  IN THIS ORDER:
; -_  += TAB [{ ]} CR ;: '" `~ \| ,< .> /? SPACE

;UNSHIFTED

PUNN_TBL	DB	"-=",9,"[]",13,";'`\,./",32

;SHIFTED

PUNS_TBL	DB	'_+',9,'{}',13,':"~|<>?',32

;GOLD

PUNG_TBL	DB	G_BRKH,0,0,G_PCMD,0,G_PARMRK,G_FIG,G_REPL,G_DATE,0
	DB	G_SRCH,G_CONTS,G_CONTSS,G_NBSPC



;NUMERIC PAD TRANS TABLES.  IN NUMERIC ORDER.

;UNSHIFTED

NUMN_TBL	DB	G_ADV,G_BAK,G_LINE,G_UCASE,G_WORD,G_PARA,G_BOLD,G_SENT
	DB	G_TABP,G_UDL

;SHIFTED

NUMS_TBL	DB	0,0,G_DN,G_PGDN,G_LEFT,G_HALT,G_RIGHT,G_HPUSH,G_UP,G_PGUP

;GOLD

NUMG_TBL	DB	G_GADV,G_GBAK,0,G_GUCASE,0,0,G_GBOLD,0,0,G_GUDL


DATA  	ENDS

PAGE
CODE	SEGMENT PUBLIC 'CODE'
	ASSUME	CS:CODE,DS:DATA

;INITIALIZES THE LOCAL KEYBOARD BUFFER, AND CHANGES KEYBOARD INT VECTOR
;TO POINT TO LOCAL KEYBOARD ISR.  ALSO CHANGES CLOCK VECTOR TO LOCAL ISR.

INIT_KB:
	CALL	INIT_KB_BUF
	SAVE	ES,DS
	CLI				;INTERRUPTS OFF WHILE WE MESS AROUND
	ZAP	AX
	MOV	ES,AX
	MOV	AL,BYTE PTR ES:417H	;GET CURRENT KBD STATUS
	TESTZ	AL,40H,INI1		;IS CAPS LOCK ALREADY ON?
	MOV	KB_FLAG,CPSLK_FG	;YES...SET IT FOR WPS-PC
INI1:	STI				;INTERRUPTS BACK ON
	MOV	AL,9			;KB VECTOR 9
	MSDOS	53			;GET KB VECTOR IN ES:BX
	MOV	AX,ES			;SAVE OLD INT 9
	MOV	OLD_KBD,AX
	MOV	OLD_KBD[2],BX
	MOV	AL,1CH
	MSDOS	53			;GET CLOCK VECTOR IN ES:BX
	MOV	AX,ES
	MOV	OLD_CLK,AX
	MOV	OLD_CLK[2],BX
	MOV	AL,9			;KB VECTOR 9
	MOV	DX,OFFSET KB_ISR
	PUSH	CS
	POP	DS
	MSDOS	37			;SET UP OUR VECTOR
	MOV	AL,1CH			;CLOCK VECTOR 8
	MOV	DX,OFFSET CLK_ISR
	MSDOS	37			;SET UP OUR VECTOR
	POP	DS
	MOV	AX,-1			;FFFF
	MOV	ES,AX
	MOV	AL,ES:0EH		;GET CPU TYPE BYTE
	MOV	CPU_TYPE,AL
	RESTORE	ES			
;	CALL	UPD_KBD			;TURN OFF LITES ON AT
	RET				;DONE		

;RESETS KBD AND CLOCK VECTORS TO SAVED VALUES

RESET_KB:
;	CALL	UPD_KBD			;RESTORE AT LIGHTS
	CALL	TONE_OFF		;TURN OFF THE BEEP, JUST IN CASE
	SAVE	DS,ES
	CLI				;HOLD INTERRUPTS
	ZAP	AX
	MOV	ES,AX
	MOV	AH, BYTE PTR ES:417H	;GET OLD KB STATUS
	AND	AH, NOT 40H		;MASK OFF OLD CAPS LOCK
	TESTZ	KB_FLAG,CPSLK_FG,RSET1	;WERE WE IN CAPS LOCK?
	OR	AH,40H			;YES...SET CAPS LOCK
RSET1:	MOV	BYTE PTR ES:417H,AH	;RESTORE KB FLAGS
	STI				
	MOV	AX,OLD_KBD		;GET OLD INT 9 VECTOR
	MOV	DX,OLD_KBD[2]			
	PUSH	DS
	MOV	DS,AX
	MOV	AL,9			;VECTOR 9
	MSDOS	37			;SET VECTOR
	POP	DS
	MOV	AX,OLD_CLK		;GET OLD INT ICH VECTOR
	MOV	DX,OLD_CLK[2]	
	MOV	DS,AX
	MOV	AL,1CH			;VECTOR 1CH
	MSDOS	37			;SET VECTOR
	RESTORE	DS,ES
	RET


;INITIALIZE LOCAL KBD BUFFER POINTERS.

INIT_KB_BUF:

	MOV	BUF_HEAD,OFFSET KB_BUF
	MOV	BUF_TAIL,OFFSET KB_BUF
	RET

;CALLED BY ANY ROUTINE TO GET NEXT CHAR FROM BUFFER IN WPS-PC FORMAT.
;RETURNED IN AX: AL=CHAR, AH=FLAG.  IF AH>0 CHAR IN AL IS WPS FUNCTION.
;AH=WPS GOLD NUMBER +1.
;IF AH=0 CHAR IN AL IS ASCII.  RETURNS IMMEDIATELY.  DOES NOT HANG.

KBD_CHAR:
	CLI				;NO INTERRUPTS
	ZAP	AX			;RETURNS AX=0 IF NO CHAR
	MOV	BX,BUF_HEAD		;PTR TO NEXT CHAR
	IFNE	BX,BUF_TAIL
	   MOV	AX,[BX]			;LOAD CHAR IN AL, FLAG IN AH
	   CALL	BUMP_BX			;NEXT POSIT IN BUF
	   MOV	BUF_HEAD,BX
	IFEND
	STI				;INTERRUPTS BACK ON
	RET

PAGE
;WPS-PC KEYBOARD ISR.  STORES CHARS IN KBD BUFFER 2 BYTES AT A TIME.  HIGH
;ORDER BYTE IS NON-ZERO IF CHAR IS A GOLD FUNCTION OR FUNCTION KEY.  HIGH
;BYTE IS DIGIT N (1-255) FOR CERTAIN GOLD FUNCTIONS.  IF HIGH BYTE IS 0,
;LOW BYTE IS ASCII CHAR IN RANGE 32-255.

KB_ISR:
	ASSUME	CS:CODE,DS:DATA,ES:DATA

	SAVE	AX,BX,CX,DX,DI,SI,DS,ES

	MOV	AX,DATA			;INIT SEGMENT PTRS
	MOV	DS,AX
	MOV	ES,AX
	IN	AL,KB_DATA		;READ KBD SCAN CODE
	MOV	AH,AL			;SAVE IT
	IN	AL,SYS_CTL		;NOW RESET KBD
	OR	AL,80H
	OUT	SYS_CTL,AL
	AND	AL,7FH
	OUT	SYS_CTL,AL
	MOV	AL,AH			;SCAN CODE BACK TO AL

	MOV	CL,KB_FLAG		;HOLD FLAG IN CL
	IFNE	AL,CPSLK_BRK,K1		;NOT CAPS LOCK BREAK
K0:	XOR	CL,CPSLK_FG    		;ELSE REVERSE CAPS LOCK STATE
;	CALL	UPD_KBD			;UPDATE SHIFT LOCK LIGHT ON AT  ;012
	JR	K4A
K1:	IFNE	AL,CTRL_MAK,K2		;NOT CTRL_MAK
	IFNZ	CTRL_SHIFT,KEX1,L	;SHIFT LOCK MODE...IGNORE
	OR	CL,CTRL_FG		;ELSE SHOW CTRL ON
	GOTO	KEX1
K2:	IFNE	AL,CTRL_BRK,K3		;NOT CTRL_BRK
	IFNZ	CTRL_SHIFT,K0		;SHIFT LOCK MODE...TREAT AS CAPS LOCK
	AND	CL,NOT CTRL_FG 		;ELSE TURN OFF CTRL
	JR	K4A
K3:	IFEQ	AL,LSHFT_MAK,K4		;NOT LEFT SHIFT MAK
	IFNE	AL,RSHFT_MAK,K5		;NOT RIGHT SHIFT MAK
K4:	OR	CL,SHFT_FG		;SHOW SHIFT IN EFFECT
K4A:	MOV	KB_FLAG,CL
	IFNE	SCREEN_MODE,"E",K4C
	IFZ	STATUS_FLAG,K4C		;DONT UPDATE IF NO STATUS
	MOV	AL,25			;DN ARROW
	AND	CL,SHFT_FG OR CPSLK_FG
	JZ	K4B 			;SHIFT OFF	
	MOV	AL,24			;UP ARROW
K4B:	MOV	AH,70H			;REVERSE
	IFEQ	STATUS_FLAG,1,K4D
	MOV	AH,7			;NORMAL
K4D:	MOV	BX,VIDBASE
	MOV	ES,BX
	MOV	DI,158
	STOSW				;STORE THE CHAR TO SCREEN BUFFER
K4C:	GOTO	KEX2
K5:	IFEQ	AL,RSHFT_BRK,K5A	;NOT RIGHT SHIFT BRK
	IFNE	AL,LSHFT_BRK,K6		;NOT LEFT SHIFT BRK
K5A:	AND	CL,NOT SHFT_FG 		;NO LONGER IN SHIFT MODE
	JR	K4A
K6:	IFNE	AL,ALT_MAK,K7		;NOT ALT MAK
	OR	CL,ALT_FG		;ELSE SHOW ALT MODE
	MOV	ADIGIT,0		;ZERO ALT DIGIT
	GOTO	KEX
K7:	IFNE	AL,ALT_BRK,K8		;NOT ALT BREAK
	AND	CL,NOT ALT_FG  		;ELSE NOT IN ALT MODE
	MOV	AL,ADIGIT		;GET ALT DIGIT
	IFLT	AL,32,KEX,L		;NOTHING TO REPORT
	ZAP	AH			;TREAT AS ASCII CHAR
	GOTO	KSTORE

K8:	IFGE	AL,128,KEX1,L		;IGNORE ALL BREAK KEYS FROM HERE ON
	MOV	BX,OFFSET XLINT		;TRANSLATE TO INTERNAL FORMAT
	XLAT				;DO IT
	MOV	AH,AL			;SAVE INT FORMAT CODE
	IFNE	AL,DEL_KEY,K9		;NOT DELETE KEY
	MOV	AL,CL	       		;IS THIS CTRL-ALT-DEL (RESET)?
	AND	AL,CTRL_FG OR ALT_FG	;MASK DOWN TO ALT-CTRL FLAG BITS
	CMP	AL,CTRL_FG OR ALT_FG	;BOTH SET?
	JNE	K9			;NO...DONT RESET
	MOV	AL,EOI
	OUT	INT_CTRL,AL
	CALL	RESET_KB		;RESET THE INT VECTOR
	GOTO	ABORT
K9:	MOV	AL,AH			;RESTORE INTERNAL CODE
	IFNZ	AL,K10			;NOT GOLD KEY
	TESTNZ	CL,GOLD_FG,K9A		;ALREADY HAVE GOLD KEY?
	OR	CL,GOLD_FG		;SHOW IN GOLD FUNCTION
	GOTO	KEX1
K9A:	MOV	G_N_G_FG,1		;POSSIBLE GOLD-N-GOLD FUNCTION
	GOTO	KEX1
K10:	IFNE	AL,NP5_KEY,K11		;NOT PRINT SCREEN
	CALL	TST_SHFT_KB51		;SHIFT MODE ON NUM PAD?
	JZ	K11			;NO
;	TESTZ	CL,SHFT_FG,K11		;NOT SHIFT MODE
	IFNZ	PRINTER_BUSY,KEX,L	;NO PRINT SCREEN IF PRINTER BUSY
	MOV	AL,EOI			;END OF INTERRUPT
	OUT	INT_CTRL,AL
	INT	5			;PRINT SCREEN
	GOTO	KEXNC

K11:	ZAP	AH			;PASS BACK 0 FOR NORMAL ASCII
	TESTZ	CL,ALT_FG,K11D		;NOT ALT
	IFNE	AL,"U",K30,L		;NOT ALT-UDK
	MOV	AL,G_DEFUDK		;ELSE SHOW UDK DEF INVOCATION
	GOTO	KGSTORE
K11D:	TESTZ	CL,GOLD_FG,K12		;GOLD KEY NOT SET
	IFNE	AL,HALT_KEY,K11B	;GOLD HALT?
K11A:	CALL	INIT_KB_BUF		;RESET KB BUF
	MOV	HALT_FG,1		;SHOW IMMEDIATE HALT REQUESTED
	GOTO	KEX

K11B:	TESTZ	CL,SHFT_FG,K11C		;GOLD SHIFT?
	IFNE	AL,30,K11C		;YES...INV HYPHEN?
	MOV	AL,G_INVH		;YES
	GOTO	KGSTORE

K11C:	CALL	TST_SHFT_KB51		;SHIFT MODE, NUM PAD?
	JZ	K12			;NO
	IFNE	AL,17,K12		;HYPHEN PULL?
	MOV	AL,G_HPULL		;YES
	GOTO	KGSTORE

K12:	TESTNZ	CL,CTRL_FG,K40,L	;POSSIBLE WORD STAR COMMAND
	IFLT	AL,"A",K15		;NOT ALPHA
	IFGT	AL,"Z",K15
	TESTNZ	CL,GOLD_FG,K13		;ALPHA GOLD FUN
	TESTNZ	CL,SHFT_FG,KSTORE,L	;USE UPPER CASE
	TESTNZ	CL,CPSLK_FG,KSTORE,L	;DITTO
	OR	AL,"a"-"A"		;ELSE MAKE IT LOWER CASE
	GOTO	KSTORE
K13:	SUB	AL,"A"			;ELSE INDEX ALPHA-GOLD TABLE
	MOV	BX,OFFSET ALFG_TBL
	GOTO	KGXLAT

K15:	IFLT	AL,"0",K20		;DIGIT?
	IFGT	AL,"9",K20
	IFNZ	RULR_FG,K16		;SPECIAL RULER MODE?
	TESTZ	CL,GOLD_FG,K19		;NOT GOLD-DIGIT
	SUB	AL,"0"
	MOV	DL,GDIGIT
	MOV	DIG_FG,1		;SHOW AT LEAST 1 DIGIT (EVEN "0")
	CALL	CALC			;ADD NEW DIGIT
	MOV	GDIGIT,AL
	GOTO	KEX1	
K16:	SUB	AL,"0"-1		;CONVERT RANGE
	TESTZ	CL,SHFT_FG,K17		;SHIFTED CODE?
	ADD	AL,10			;YES...ADD MORE
K17:	MOV	AH,AL			;PASS NUMBER OF KEY (1 ORIGIN)
	MOV	AL,G_RCODE		;RULER CODE
	GOTO	KSTORE
K19:	TESTZ	CL,SHFT_FG,KSTORE,L	;IF NOT SHIFTED, STORE ASCII VAL
	SUB	AL,"0"			;ELSE XLAT TO TOP ROW SYMBOLS
	MOV	BX,OFFSET DIGS_TBL
	GOTO	KXLAT

K20:	IFLT	AL,30,K25		;PUNCTUATION?
	IFGT	AL,47,K25		;NO
	SUB	AL,30
	TESTNZ	CL,GOLD_FG,K21 		;GOLD
	TESTNZ	CL,SHFT_FG,K22		;SHIFT
	MOV	BX,OFFSET PUNN_TBL
	GOTO	KXLAT

K21:	MOV	BX,OFFSET PUNG_TBL
	GOTO	KGXLAT

K22:	MOV	BX,OFFSET PUNS_TBL
	GOTO	KXLAT

K25:	IFLT	AL,20,K30		;SPECIAL WPS FUN KEY?
	IFGT	AL,29,K30		;NO
	SUB	AL,20
	TESTNZ	CL,GOLD_FG,K26		;GOLD FUN?
	TESTNZ	CL,SHFT_FG,K27
K25A:	MOV	BX,OFFSET SPLN_TBL
	GOTO	KGXLAT
K26:	IFZ	DIG_FG,K26A		;NO GOLD DIGITS...ALL OK
	IFZ	G_N_G_FG,K25A		;NO GOLD-N-GOLD, USE NORMAL TABLE
K26A:	MOV	BX,OFFSET SPLG_TBL
	GOTO	KGXLAT
K27:	MOV	BX,OFFSET SPLS_TBL
	GOTO	KGXLAT

K30:	IFLT	AL,10,K35		;NUMERIC PAD CHAR?
	IFGT	AL,19,K35		;NO
	SUB	AL,10
	TESTZ	CL,ALT_FG,K30A		;ALT KEY DOWN?
	MOV	DL,ADIGIT		;YES...ADD ANOTHER DIGIT
	CALL	CALC
	MOV	ADIGIT,AL
	GOTO	KEX

K30A:	TESTNZ	CL,GOLD_FG,K31		;GOLD
;	TESTNZ	CL,SHFT_FG,K32		;SHIFTED
	CALL	TST_SHFT_KB51		;SHIFT MODE, NUM PAD?
	JNZ	K32
	MOV	BX,OFFSET NUMN_TBL	;TRANS TABLE
	GOTO	KGXLAT

K31:	MOV	BX,OFFSET NUMG_TBL
	GOTO	KGXLAT

K32:	MOV	BX,OFFSET NUMS_TBL
	GOTO	KGXLAT	

K35:	IFLT	AL,128,KEX		;FUNCTION KEY?
	IFGT	AL,137,KEX		;NO
	MOV	AH,255
	TESTZ	CL,SHFT_FG,K35A		;SHIFT?
	ADD	AL,10
	JMP	SHORT KSTORE
K35A:	TESTZ	CL,CTRL_FG,K35B		;CTRL?
	ADD	AL,30
	JMP	SHORT KSTORE
K35B:	TESTZ	CL,ALT_FG,KSTORE	;ALT?
	ADD	AL,20
	JMP	SHORT KSTORE

K40:	IFLT	AL,"A",KEX		;CTRL CHAR OUT OF RANGE
	IFGT	AL,"Z",K35		;MIGHT BE CTRL-FUN KEY
	SUB	AL,"A"			;WORD STAR TABLE
	MOV	BX,OFFSET WS_TBL

KGXLAT:	XLAT
KGSTORE:
	MOV	AH,GDIGIT		;WPS FUNCTION
	INC	AH
	IFNZ	DIG_FG,KSTORE		;NUMBER IN AH IS VALID
	MOV	AH,255			;ELSE SHOW GOLD FUN W/O DIGIT
	JR	KSTORE

KXLAT:	XLAT
KSTORE:	IFZ	AL,KEX			;NOTHING TO PASS BACK
	MOV	BX,BUF_TAIL		;PTR TO NEXT PLACE TO USE IN BUFFER
	MOV	SI,BX
	CALL	BUMP_BX			;ADJUST TO NEXT POSITION
	IFEQ	BX,BUF_HEAD,BUF_FULL	;BUFFER OVERFLOW?
	MOV	[SI],AX			;STORE CHAR AND FLAG INTO BUFFER
	MOV	BUF_TAIL,BX		;STORE PTR

	MVAL	GDIGIT,0		;ZERO DIGIT
	MVAL	DIG_FG			;CLEAR FLAGS
	MVAL	G_N_G_FG
KEX:	AND	CL, NOT GOLD_FG		;CLEAR GOLD FLAG
KEX1:	MOV	KB_FLAG,CL		;SAVE FLAG
KEX2:	MOV	AL,EOI			;END OF INT
	OUT	INT_CTRL,AL		;INT COMPLETE

KEXNC:	RESTORE	AX,BX,CX,DX,DI,SI,DS,ES
	IRET				;AND AWAY WE GO ... 


;COME HERE TO ABORT WPS-PC AND ATTEMPT RETURN TO DOS, USING DOS HALT INT.

ABT:	STOP				;BACK TO DOS, WE HOPE

ABORT:	CALL	RESET_KB		;RESTORE KB VECTOR
	MOV	BP,SP			;POINT TO STACK
	MOV	[BP+16],OFFSET ABT	;POINT RETURN ADDR TO OUR ABORT CODE
	MOV	AX,CS
	MOV	[BP+18],AX		;MAKE SURE WE COME TO THIS CODE SEG
	JR	KEX			;NOW GET OUTTA HERE


;ADD NEW DIGIT TO NUMBER IN DL.  RESULT IN AL.

CALC:	XCHG	AL,DL
	MOV	AH,10			;MULTIPLY NUMBER IN AL BY 10
	MUL	AH
	ZAP	DH
	ADD	AX,DX
	RET

;BUMPS BX AND CHECKS FOR END OF KBD BUFFER.  RESETS TO BEGINNING IF END.

BUMP_BX:
	DINC	BX
	CMP	BX,OFFSET KB_BUF_END
	JB	BUMP1
	MOV	BX,OFFSET KB_BUF	;ELSE RESET TO TOP OF BUFFER
BUMP1:	RET



BUF_FULL:
	AND	CL, NOT GOLD_FG		;CLEAR GOLD FLAG
	MOV	KB_FLAG,CL		;SAVE FLAG
	MOV	AL,EOI			;END OF INT
	OUT	INT_CTRL,AL		;INT COMPLETE
	CALL	BEEP$$			;BEEP ON BUFFER FULL
	GOTO	KEXNC

;REVERSES THE SENSE OF THE SHIFT FLAG FOR CURSOR PAD MODE ON KB5151 KBD.

TST_SHFT_KB51:
	MOV	DL,CL			;GET FLAGS
	AND	DL,SHFT_FG		;CHECK SHIFT MODE
	XOR	DL,KB5151		;REVERSE SENSE IF KB5151
	RET				;RETURN WITH Z SET PROPERLY

PAGE
;CLOCK ISR AND ASSOCIATED ROUTINES

BEEP$$:
	CALL	TONE_ON			;TURN ON TONE
	MOV	BEEPS,BP_LEN		;NUMBER OF TICKS PER BEEP (18/SEC)
	RET

;CALLED WITH ADDRESS OF ROUTINE IN DX AND NUMBER OF CLOCK TICKS (18 PER SEC)
;UNTIL THAT ROUTINE IS TO BE CALLED IN AX.  ROUTINE MUST SAVE AND RESTORE ALL 
;REGS IT USES.

SET_EVENT$$:
	PUSH	AX
	CALL	CLR_EVENT$$		;MAKE SURE NO ENTRY IN LIST
	POP	AX
	CALL	SET_PTR			;POINT TO EVENT TABLE
SET1:	CMP	WORD PTR [BX],0
	JNZ	SET2			;ACTIVE ENTRY
	MOV	[BX],AX			;ELSE PUT IN TICK COUNT
	MOV	2[BX],DX		;AND ROUTINE ADDRESS
	RET
SET2:	ADD	BX,4			;ELSE TRY NEXT ENTRY
	LOOP	SET1
	STC				;SHOW FAILED TO MAKE ENTRY
	RET

;CALLED TO CLEAR EVENT, WITH ADDRESS IN DX.

CLR_EVENT$$:
	CALL	SET_PTR			;POINT TO EVENT TABLE
CLR1:	IFNE	DX,2[BX],CLR2		;ROUTINES MATCH?
	ZAP	AX
	MOV	[BX],AX
	MOV	2[BX],AX
	RET				;DONE
CLR2:	ADD	BX,4			;ELSE TRY NEXT ENTRY
	LOOP	CLR1
	STC
	RET				;FAILED

;INIT ROUTINE USED BY EVENT ROUTINES.

SET_PTR:
	MOV	BX,OFFSET EVENT_TBL
	MOV	CX,NUM_EVENTS
	RET	

;CLOCK ISR

CLK_ISR:
	SAVE	AX,BX,CX,DS
	MOV	AX,DATA
	MOV	DS,AX
	CALL	SET_PTR
CLK0:	CMP	WORD PTR [BX],0
	JZ	CLK1			;NOT ACTIVE EVENT
	DEC	WORD PTR [BX]		;ELSE GIVE IT A TICK
	JNZ	CLK1			;NO TIMEOUT YET
	SAVE	BX,CX,DX,SI,DI,BP
	CALL	WORD PTR 2[BX]		;ELSE CALL ROUTINE
	RESTORE	BX,CX,DX,SI,DI,BP
	MOV	WORD PTR 2[BX],0	;MAKE SURE ZAP THE ENTRY
CLK1:	ADD	BX,4			;BUMP PTR
	LOOP	CLK0
	IFZ	BEEPS,CLK2		;NO BEEP TIMING GOING ON
	DEC	BEEPS
	JNZ	CLK2			;NOT YET
	CALL	TONE_OFF		;ELSE TURN OFF NOISE
CLK2:	RESTORE	AX,BX,CX,DS
	IRET				;END OF CLOCK PROCESSING


;TONE PRIMITIVES

TONE_ON:
	MOV	AL,TONE_CTL		;CONTROL TIMER
	OUT	TIMR_CTL,AL
	MOV	CX,TONE			;CX=CLOCK RATE/FREQ HZ
	MOV	AL,CL			;OUTPUT COUNT TO TIMER
	OUT	TIMR_DATA,AL
	MOV	AL,CH
	OUT	TIMR_DATA,AL
	IN	AL,SYS_CTL		;START THE TIMER AND ENABLE SPKR
	OR	AL,3
	OUT	SYS_CTL,AL
	RET

TONE_OFF:
	PUSH	AX
	IN	AL,SYS_CTL		;TURN OFF SPEAKER AND TIMER
	AND	AL,NOT 3
	OUT	SYS_CTL,AL
	POP	AX
	RET

;UPDATES THE LIGHTS ON THE AT KEYBOARD

UPD_KBD:
	IFNE	CPU_TYPE,0FCH,RET	;NOT AN AT...IGNORE
	MOV	AL,0EDH			;SET/RESET KBD COMMAND
	CALL	XMIT
	ZAP	AL
	TEST	KB_FLAG,CPSLK_FG	;CAPS LOCK OFF?
	JZ	UPD1			;YES
	MOV	AL,4			;ELSE TURN IT ON
UPD1:	CALL	XMIT
	RET

XMIT:	XCHG	AL,AH			;SAVE CMD PARAM
	ZAP	CX
X1:	IN	AL,64H
	TEST	AL,2			;DATA WAITING FOR CTRLR
	LOOPNZ	X1			;YES, WAIT
	RCXZ				;ELSE TIMEOUT
	XCHG	AL,AH
	OUT	60H,AL			;SEND TO KBD
	ZAP	CX
X2:	IN	AL,64H
	TEST	AL,2			;HAS CONTROLLER READ DATA YET?
	LOOPNZ	X2			;NO
	RCXZ
	ZAP	CX
X3:	IN	AL,64			;NOW WAIT FOR ACK
	TEST	AL,1			;DID KBD RESPOND YET?
	LOOPNZ	X3			;NO
	RCXZ
	IN	AL,60H			;GET REPLY
	RET


CODE	ENDS

	END
